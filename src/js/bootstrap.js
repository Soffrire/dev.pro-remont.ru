try {
  window.$ = window.jQuery = require('jquery')
  require('magnific-popup')
  require('inputmask/dist/jquery.inputmask.min')
  require('smartwizard/dist/js/jquery.smartWizard.min')
  require('jquery-validation/dist/jquery.validate.min')
} catch (e) { }

window.axios = require('axios')
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
