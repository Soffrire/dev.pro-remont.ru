import Swiper from 'swiper'

const reviewsSlider = {
  init() {
    this.$wrapper = $('.js-reviews-slider')

    this.initSlider()
  },
  initSlider() {
    // eslint-disable-next-line no-new
    new Swiper(this.$wrapper, {
      loop: true,
      slidesPerView: 3,
      spaceBetween: 70,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      pagination: {
        el: '.swiper-pagination'
      },
      breakpoints: {
        320: {
          slidesPerView: 1,
          spaceBetween: 0
        },
        425: {
          slidesPerView: 1,
          spaceBetween: 0
        },
        560: {
          slidesPerView: 1,
          spaceBetween: 0
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 50
        }
      }
    })
  }
}

const aboutUsSlider = {
  init() {
    this.$wrapper = $('.js-about-us-slider')

    this.initSlider()
  },
  initSlider() {
    // eslint-disable-next-line no-new
    new Swiper(this.$wrapper, {
      loop: true,
      slidesPerView: 3,
      spaceBetween: 20,
      navigation: {
        nextEl: this.$wrapper.find('.slider__btn-next'),
        prevEl: this.$wrapper.find('.slider__btn-prev')
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      },
      breakpoints: {
        320: {
          slidesPerView: 1,
          spaceBetween: 0
        },
        425: {
          slidesPerView: 1,
          spaceBetween: 0
        },
        560: {
          slidesPerView: 1,
          spaceBetween: 0
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 40
        }
      }
    })
  }
}

const calcSlider = {
  init() {
    this.$wrapper = $('.js-calc-slider')

    this.initSlider()
  },
  initSlider() {
    // eslint-disable-next-line no-new
    new Swiper(this.$wrapper, {
      loop: false,
      cssMode: true,
      slidesPerView: 1,
      spaceBetween: 20,
      navigation: {
        nextEl: this.$wrapper.find('.calculator-btn--next'),
        prevEl: this.$wrapper.find('.calculator-btn--prev')
      },
      mousewheel: true
    })
  }
}

const portfolioSlider = {
  init() {
    this.$wrapper = $('.js-portfolio-slider')

    this.initSlider()
  },
  initSlider() {
    // eslint-disable-next-line no-new
    new Swiper(this.$wrapper, {
      slidesPerView: 4,
      spaceBetween: 20,
      navigation: {
        nextEl: this.$wrapper.find('.swiper-button-next'),
        prevEl: this.$wrapper.find('.swiper-button-prev')
      },
      pagination: {
        el: this.$wrapper.find('.swiper-pagination')
      }
    })
  }
}

const initSliders = () => {
  reviewsSlider.init()
  aboutUsSlider.init()
  calcSlider.init()
  portfolioSlider.init()
}

export default initSliders
