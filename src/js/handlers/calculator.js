const calculator = {
  init() {
    this.$wrapper = $('.js-calculator')
    this.$rangeWrapper = $('.js-range')
    this.$result = this.$wrapper.find('.js-calc-result')
    this.values = {}
    this.eventHandler()
  },

  eventHandler() {
    const self = this

    window.addEventListener('DOMContentLoaded', () => {
      $('.js-calc-input').each(function() {
        const type = $(this).attr('type')
        if ( (type == 'radio') && $(this).attr('checked'))
          self.setValue($(this))
        else if (type == "range")
          self.setValue($(this))
        else if (type == 'checkbox')
          self.setValue($(this))
      })
      this.getSum()
    })

    $(document).on('change', '.js-calc-input', function() {
      self.setValue($(this))
      self.getSum()
    })

  },

  setValue($this) {
    if ($this.attr('type') == 'checkbox' && !$this[0].checked)
      this.values[$this.attr('name')] = 0
    else
      this.values[$this.attr('name')] = Number($this.val())
  },

  getSum() {
    const values = this.values
    values["sumSLide1"] = values["type"] * values["S"]
    values["sumSlide2"] = values["sumSLide1"] + (values['floor'] * values["S"]) + (values["ceiling"] * values["S"]) + (values["S"] * values["walls"]) + values['other-bowl'] + (values['other-floors'] * values["S"]) + values['other-defend'] + values['other-home']

    $(this.$result).html(values["sumSlide2"])
    console.log(values)
  }
}

export default calculator
