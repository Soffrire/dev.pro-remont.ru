function pageWidget(pages) {
  const widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>')

  widgetWrap.prependTo("body")

  for (var i = 0; i < pages.length; i++) {
    $(`<li class="widget_item"><a class="widget_link" href="/${pages[i]}.html">${pages[i]}</a></li>`)
      .appendTo('.widget_list')
  }
  const widgetStyle = $(
    '<style>body {position:relative} .widget_wrap{position:fixed;top:0;left:0;z-index:9999;padding:10px 20px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>'
  )
  widgetStyle.prependTo('.widget_wrap')
}

pageWidget(['index', '404', 'privacy', 'ui'])

$(document).ready(function () {
  $('.ba-slider').each(function () {
    var cur = $(this)
    var width = cur.width() + 'px'

    cur.find('.ba-resize img').css('width', width)
    drags(cur.find('.ba-handle'), cur.find('.ba-resize'), cur)
  })

});

$(window).resize(function () {
  $('.ba-slider').each(function () {
    var cur = $(this)
    var width = cur.width() + 'px'
    cur.find('.ba-resize img').css('width', width)
  })
})

function drags(dragElement, resizeElement, container) {

  // Initialize the dragging event on mousedown.
  dragElement.on('mousedown touchstart', function (e) {
    dragElement.addClass('draggable')
    resizeElement.addClass('resizable')

    // Check if it's a mouse or touch event and pass along the correct value
    var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX

    // Get the initial position
    var dragWidth = dragElement.outerWidth(),
        posX = dragElement.offset().left + dragWidth - startX,
        containerOffset = container.offset().left,
        containerWidth = container.outerWidth();

    // Set limits
    var minLeft = containerOffset + 10
    var maxLeft = containerOffset + containerWidth - dragWidth - 10

    // Calculate the dragging distance on mousemove.
    dragElement.parents().on("mousemove touchmove", function(e) {
      // Check if it's a mouse or touch event and pass along the correct value
      var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX

      var leftValue = moveX + posX - dragWidth

      // Prevent going off limits
      if ( leftValue < minLeft ) {
        leftValue = minLeft
      } else if (leftValue > maxLeft) {
        leftValue = maxLeft
      }

      // Translate the handle's left value to masked divs width.
      var widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + '%'

      // Set the new values for the slider and the handle.
      // Bind mouseup events to stop dragging.
      $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function () {
        $(this).removeClass('draggable')
        resizeElement.removeClass('resizable')
      })

      $('.resizable').css('width', widthValue)
    }).on('mouseup touchend touchcancel', function () {
      dragElement.removeClass('draggable')
      resizeElement.removeClass('resizable')
    })
    e.preventDefault()
  }).on('mouseup touchend touchcancel', function (e) {
    dragElement.removeClass('draggable')
    resizeElement.removeClass('resizable')
  })
}
