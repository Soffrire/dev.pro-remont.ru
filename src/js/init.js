/* eslint-disable no-new */
import addInputMaskPhone from './handlers/form-elements/phone-mask'
import CustomSelect from './handlers/form-elements/custom-select'
import MultipleCustomSelect from './handlers/form-elements/multiple-custom-select'

import calculator from './handlers/calculator'
import formSender from './handlers/form-sender'
import initSliders from './handlers/sliders'
import initMapHandlers from './handlers/map-handler'

import { modalHandler, modalGallery } from './handlers/modals'

function initApp(newChild) {
  window.$body = $('.js-body')

  calculator.init()

  /**
   *  Добавление масок к номерам телефонов
   */
  addInputMaskPhone()

  /**
   * Обработчик поведение списков выбора
   */
  $('.js-custom-select').each(function () {
    new CustomSelect($(this))
  })

  /**
   * Обработчик поведение множественных списков выбора
   */
  $('.js-custom-multiple-select').each(function () {
    new MultipleCustomSelect($(this))
  })

  /**
   * Обработчик отправки форм
   */
  formSender.init()

  /**
   * Инициализация модальных окон
   */
  modalHandler.init()

  /**
   * Инициализация модальных окон для просмотра фото
   */
  modalGallery.init()

  /**
   * Инициализация слайдеров
   */
  initSliders()

  /**
   * Инициализация карт
   */
  initMapHandlers()

  $('.js-phone-mask').inputmask({
    mask: '+7 (999) 999-99-99',
    placeholder: '+7 (---) --- -- --'
  })

  $(document).each(function () {
    if ($(window).width() < 768)
      $('.input-field__btn').text('').append('<svg class="icon icon-check"><use xlink:href="#check"></use></svg>')
    if ($(window).width() < 425)
      $('.social').removeClass('social--invert')
  })

  // $('.status-bar__item').on('click', function () {
  //   $(this).toggleClass('status-bar__item--active')
  // })

  $(document).ready(function () {
    $('#smartwizard').smartWizard({
      anchorSettings: {
        removeDoneStepOnNavigateBack: true
      },
      transition: {
        animation: 'fade-in',
        speed: '400',
        easing: ''
      },
      lang: {
        next: 'Далее',
        previous: 'Назад'
      },
      onFinish: function () {
        $('.form').submit()
      }
    })
  })

  $('.form').validate({
    rules: {
      username: 'required'
    }
  })

  window.addEventListener('DOMContentLoaded', () => {
    const range1 = new rSlider({
      element: '#range1',
      tick: 10
    })
    const range2 = new rSlider({
      element: '#range2',
      tick: 1
    })
  })
  class rSlider {
    constructor(args) {
      this.el = document.querySelector(args.element)
      this.min = +this.el.min || 0
      this.max = +this.el.max || 100
      this.step = +this.el.step || 1
      this.tick = args.tick || this.step
      this.addTicks()
      this.dataRange = document.createElement('div')
      this.dataRange.className = 'data-range'
      this.el.parentElement.appendChild(this.dataRange, this.el)
      this.updatePos()
      this.el.addEventListener('input', () => {
        this.updatePos()
      })
    }

    addTicks() {
      const wrap = document.createElement('div')
      wrap.className = 'range'
      this.el.parentElement.insertBefore(wrap, this.el)
      wrap.appendChild(this.el)
      const ticks = document.createElement('div')
      ticks.className = 'range-ticks'
      wrap.appendChild(ticks)
      for (let t = this.min; t <= this.max; t += this.tick) {
        const tick = document.createElement('span')
        tick.className = 'range-tick'
        ticks.appendChild(tick)
        const tickText = document.createElement('span')
        tickText.className = 'range-tick-text'
        tick.appendChild(tickText)
        tickText.textContent = t
      }
    }

    getRangePercent() {
      const max = this.el.max
      const min = this.el.min
      const relativeValue = this.el.value - min
      const ticks = max - min
      const percent = relativeValue / ticks
      return percent
    }

    updatePos() {
      const percent = this.getRangePercent()
      const left = percent * 100
      const emAdjust = percent * 3
      this.dataRange.style.left = `calc(${left}% - ${emAdjust}em)`
    }
  }
}

export { initApp }
